const knex = require('../database/db');

const deletedata = {
    async del(req, res) {
        const delete_id = req.params.id;
        const chk = await knex('createdata').select('ID').where({ ID: req.params.id });
        if (chk.length == 0) {
            res.json("User does not exist");
        } else {
            const deltedata = await knex('createdata').where({ID : delete_id}).delete();
            if (deltedata) {
                res.json("Data delete");
            } else {
                res.json("Data not deleted");
            }
        }
    }
}

module.exports = deletedata;
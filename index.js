const express = require('express');
const app = express();
const port= 3000
var bodyParser = require('body-parser')
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
const data = require('./router/router');
app.use('/',data)
app.listen(port,()=>{
    console.log("Server run successfull")
})
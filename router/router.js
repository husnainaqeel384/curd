const express = require('express');
const router = express.Router();
const controll = require('../controller/createcontroller');
const readcontroll =require('../controller/readcontroller');
const updatecontroll = require('../controller/updatecontrooler');
const deletecontroll = require('../controller/deletecontroller');
router.get('/:name/:class/:phone', controll.CretedData);
router.post('/:name', readcontroll.Reads);
router.post('/', readcontroll.alldata);
router.put('/:id', updatecontroll.changeddata);
router.delete('/:id',deletecontroll.del);

module.exports = router;